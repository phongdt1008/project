/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.freemoney.redis;

import bliss.lib.framework.redis.RedisFactory;
import bliss.lib.framework.util.ConvertUtils;
import java.util.Calendar;
import kplay.service.gamemanager.config.ModuleConfig;
import org.apache.commons.lang.StringUtils;
import redis.clients.jedis.Jedis;

/**
 *
 * @author baotn
 */
public class VideoAdCA {

    public static final String VIDEO_AD = "freemoney:video_ad";

    Jedis jedis = null;
    RedisFactory client = null;

    public static int getVideoAdCount(int userId) {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();

            int dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

            String key = VIDEO_AD + ":" + dayOfMonth + ":" + userId;

            if (!jedis.exists(key)) {
                setVideoAdCount(userId);
            }

            String strCount = jedis.get(key);
            if (StringUtils.isEmpty(strCount)) {
                return 0;
            }

            int count = ConvertUtils.toInt(strCount, 0);

            return count;
        } catch (Exception ex) {
            return 0;
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
    }

    public static boolean decrVideoAdCount(int userId) {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();

            int dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

            String key = VIDEO_AD + ":" + dayOfMonth + ":" + userId;

            jedis.decrBy(key, 1);

            return true;
        } catch (Exception ex) {
            return false;
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
    }

    public static boolean setVideoAdCount(int userId) {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();

            int dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

            String key = VIDEO_AD + ":" + dayOfMonth + ":" + userId;

            jedis.set(key, "5");
            jedis.expire(key, 86400);

            return true;
        } catch (Exception ex) {
            return false;
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
    }
}
