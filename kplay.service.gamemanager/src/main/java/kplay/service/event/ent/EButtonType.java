/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.event.ent;

/**
 *
 * @author baotn
 */
public enum EButtonType {
    NONE(0),
    SCENE(1),
    WEBVIEW(2),
    WEBBROWSER(3),
    IAP(4),
    PLAYNOW(5),
    SHARE(6),
    INVITE(7);

    private final int value;

    private EButtonType(int value) {
        this.value = value;
    }

    /**
     * Get the integer value of this enum value, as defined in the Thrift IDL.
     */
    public int getValue() {
        return value;
    }

    /**
     * Find a the enum type by its integer value, as defined in the Thrift IDL.
     *
     * @return null if the value is not found.
     */
    public static EButtonType findByValue(int value) {
        switch (value) {
            case 0:
                return NONE;
            case 1:
                return SCENE;
            case 2:
                return WEBVIEW;
            case 3:
                return WEBBROWSER;
            case 4:
                return IAP;
            case 5:
                return PLAYNOW;
            case 6:
                return SHARE;
            case 7:
                return INVITE;
            default:
                return null;
        }
    }
}
