/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.event.ent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author baotn
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@Table(name = "kplay.event")
public class EventEnt implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
    public String title;
    public int priority;
    public String bannerURL;
    public String showcaseURL;
    public String welcomeURL;
    public String appPlatform;
    public String appVersion;
    public String buttonTitle;
    public int buttonType;
    public String buttonData;
    public int status;
}
