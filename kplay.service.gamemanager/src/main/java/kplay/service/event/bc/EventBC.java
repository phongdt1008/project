/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.event.bc;

import bliss.lib.entity.define.EventGroup;
import bliss.lib.entity.proto.BlissObjectFactory;
import bliss.lib.entity.utils.BlissMessageUtils;
import bliss.lib.framework.thriftgen.tcommon.TSession;
import bliss.lib.framework.util.DateTimeUtils;
import java.util.ArrayList;
import java.util.List;
import kplay.service.event.ent.EventEnt;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import kplay.service.event.db.EventDA;
import kplay.service.event.utils.EventMsgUtils;
import kplay.service.gamemanager.adapter.ConnectorAdapter;
import kplay.service.trigger.bc.TriggerBC;
import kplay.service.trigger.ent.ETriggerStatus;
import kplay.service.trigger.ent.ETriggerType;
import kplay.service.trigger.ent.TriggerEnt;

/**
 *
 * @author baotn
 */
@Service
public class EventBC {

    @Autowired
    private EventDA eventDA;

    @Autowired
    private TriggerBC triggerBC;

    public EventEnt saveEvent(EventEnt event) {
        return eventDA.save(event);
    }

    public EventEnt getEvent(int id) {
        return eventDA.getOne(id);
    }

    public void getCurrentListEvent(int userId, TSession session, BlissObjectFactory.BlissObject data) {

        int appPlatform = data.getMapIntMap().get(EventGroup.Key.APP_PLATFORM_KEY);
        String appVersion = data.getMapStringMap().get(EventGroup.Key.APP_VERSION_KEY);

        List<TriggerEnt> listTrigger = triggerBC.getCurrentListTrigger(ETriggerType.EVENT.getValue(), DateTimeUtils.getMilisecondsNow(), ETriggerStatus.ENABLE.getValue());
        List<EventEnt> listEvent = new ArrayList();
        for (int i = 0; i < listTrigger.size(); i++) {
            EventEnt event = getEvent(listTrigger.get(i).refId);
            if (event.id > 0) {
                // TODO: check filter appPlatform, appVersion before add
                listEvent.add(event);
            }
        }

        BlissObjectFactory.BlissObject.Builder dataResponse = BlissObjectFactory.BlissObject.newBuilder();

        BlissObjectFactory.BlissObject.ListBlissObject.Builder eventBuilder = EventMsgUtils.addEventToBlissObject(listEvent);

        dataResponse.putMapListBlissObject(EventGroup.Key.LIST_EVENT_KEY, eventBuilder.build());

        ConnectorAdapter.sendMsgToUserId(userId, BlissMessageUtils.getBlissMessage(EventGroup.CMD.GET_CURRENT_LIST_EVENT_CMD, dataResponse.build()));
    }
}
