/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.event.utils;

import bliss.lib.entity.define.EventGroup;
import bliss.lib.entity.proto.BlissObjectFactory;
import java.util.List;
import kplay.service.event.ent.EventEnt;

/**
 *
 * @author baotn
 */
public class EventMsgUtils {

    public static BlissObjectFactory.BlissObject.ListBlissObject.Builder addEventToBlissObject(List<EventEnt> events) {
        BlissObjectFactory.BlissObject.ListBlissObject.Builder eventsBuilder = BlissObjectFactory.BlissObject.ListBlissObject.newBuilder();

        for (int i = 0; i < events.size(); i++) {
            EventEnt event = events.get(i);
            if (event != null) {
                BlissObjectFactory.BlissObject.Builder data = BlissObjectFactory.BlissObject.newBuilder();

                data.putMapInt(EventGroup.Key.ID_KEY, event.id);
                data.putMapString(EventGroup.Key.TITLE_KEY, event.title);
                data.putMapInt(EventGroup.Key.PRIORITY_KEY, event.priority);
                data.putMapString(EventGroup.Key.BANNER_URL_KEY, event.bannerURL);
                data.putMapString(EventGroup.Key.SHOWCASE_URL_KEY, event.showcaseURL);
                data.putMapString(EventGroup.Key.WELCOME_URL_KEY, event.welcomeURL);
                data.putMapString(EventGroup.Key.BUTTON_TITLE_KEY, event.buttonTitle);
                data.putMapInt(EventGroup.Key.BUTTON_TYPE_KEY, event.buttonType);
                data.putMapString(EventGroup.Key.BUTTON_DATA_KEY, event.buttonData);

                eventsBuilder.addBlissVal(data);
            }
        }

        return eventsBuilder;
    }
}
