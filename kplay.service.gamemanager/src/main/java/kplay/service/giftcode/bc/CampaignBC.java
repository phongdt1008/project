/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.giftcode.bc;

import bliss.lib.framework.util.DateTimeUtils;
import kplay.service.giftcode.db.CampaignDA;
import kplay.service.giftcode.ent.CampaignEnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 *
 * @author baotn
 */
@Service
public class CampaignBC {

    @Autowired
    private CampaignDA campainDA;

    public CampaignEnt getCampaign(int id) {
        return campainDA.getOne(id);
    }

    public boolean updateCampaign(CampaignEnt campaign) {
        try {
            CampaignEnt ent = campainDA.getOne(campaign.getId());
            ent.setClaimed(campaign.getClaimed());
            ent.setCode(campaign.getCode());
            ent.setCodeValue(campaign.getCodeValue());
            ent.setDateBegin(campaign.getDateBegin());
            ent.setDateEnd(campaign.getDateEnd());
            ent.setMaxClaim(campaign.getMaxClaim());
            ent.setName(campaign.getName());
            ent.setStatus(campaign.getStatus());
            ent.setTotal(campaign.getTotal());
            ent.setType(campaign.getType());
            campainDA.save(ent);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public CampaignEnt getAvailableMultipleCampaign(String code, long dateNow) {
        return campainDA.getAvailableMultipleCampaign(code, DateTimeUtils.getTimestampSql(dateNow));
    }

    public boolean getMultipleCampaign(String code) {
        return campainDA.getMultipleCampaign(code) != 0;
    }
}
