/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.giftcode.bc;

import bliss.lib.framework.util.DateTimeUtils;
import java.util.List;
import kplay.service.giftcode.db.CampaignDA;
import kplay.service.giftcode.ent.CampaignEnt;
import kplay.service.giftcode.ent.EGiftCodeType;
import kplay.service.giftcode.ent.GiftCodeEnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import kplay.service.giftcode.db.GiftCodeDA;
import org.springframework.stereotype.Service;

/**
 *
 * @author baotn
 */
@Service
public class GiftCodeBC {

    @Autowired
    private GiftCodeDA codeDA;

    @Autowired
    private CampaignDA campainDA;

    public GiftCodeEnt getAvailableGiftCode(int userId, String code, long dateNow) {
        CampaignEnt campaign = campainDA.getAvailableMultipleCampaign(code, DateTimeUtils.getTimestampSql(dateNow));

        if (campaign != null && campaign.id > 0) {
            List<GiftCodeEnt> listGiftCodeClaimed = codeDA.getClaimedGiftCodeByCampaign(campaign.id, userId);

            if (campaign.maxClaim > listGiftCodeClaimed.size()) {
                List<GiftCodeEnt> listGiftCode = codeDA.getAvailableMultipleGiftCode(campaign.id);

                if (listGiftCode.size() < campaign.total) {
                    GiftCodeEnt giftCode = createMultipleGiftCode(campaign);

                    if (giftCode != null && giftCode.id > 0) {
                        return giftCode;
                    }
                }
            }
        } else {
            GiftCodeEnt giftCode = codeDA.getAvailableGiftCode(EGiftCodeType.SINGLE.getValue(), code, DateTimeUtils.getTimestampSql(dateNow));

            if (giftCode != null && giftCode.id > 0) {
                campaign = campainDA.getOne(giftCode.campaignId);

                List<GiftCodeEnt> listGiftCodeClaimed = codeDA.getClaimedGiftCodeByCampaign(campaign.id, userId);

                if (campaign.maxClaim > listGiftCodeClaimed.size()) {
                    return giftCode;
                }
            }
        }
        return null;
    }

    public int claimGiftCode(int userId, String userName, GiftCodeEnt giftCode, long dateNow) {
        int codeValue = 0;

        if (giftCode != null && giftCode.id > 0) {
            giftCode.userId = userId;
            giftCode.userName = userName;
            giftCode.claimTime = dateNow;
            codeValue = giftCode.codeValue;
            codeDA.save(giftCode);

            CampaignEnt campaign = campainDA.getOne(giftCode.campaignId);
            campaign.claimed++;
            campainDA.save(campaign);
        }

        return codeValue;
    }

    public GiftCodeEnt createMultipleGiftCode(CampaignEnt campaign) {
        GiftCodeEnt giftCode = new GiftCodeEnt();
        giftCode.campaignId = campaign.id;
        giftCode.type = campaign.type;
        giftCode.code = campaign.code;
        giftCode.codeValue = campaign.codeValue;
        giftCode.userId = 0;
        giftCode.userName = null;
        giftCode.claimTime = 0;
        giftCode.dateBegin = campaign.dateBegin;
        giftCode.dateEnd = campaign.dateEnd;
        giftCode.status = campaign.status;
        codeDA.save(giftCode);

        return giftCode;
    }
}
