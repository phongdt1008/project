/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.giftcode.db;

import java.sql.Timestamp;
import kplay.service.giftcode.ent.CampaignEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author baotn
 */
@Repository
public interface CampaignDA extends JpaRepository<CampaignEnt, Integer> {

    @Query(value = "SELECT * FROM kplay.campaign WHERE type=1 and code=:code and date_begin <=:dateNow and date_end >=:dateNow and status=1", nativeQuery = true)
    public CampaignEnt getAvailableMultipleCampaign(@Param("code") String code, @Param("dateNow") Timestamp dateNow);

    // return 0: no result, 1: match code
    @Query(value = "SELECT count(1) FROM kplay.campaign WHERE type=1 and code=:code", nativeQuery = true)
    public int getMultipleCampaign(@Param("code") String code);

}
