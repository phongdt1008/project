/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.giftcode.db;

import java.sql.Timestamp;
import java.util.List;
import kplay.service.giftcode.ent.EGiftCodeType;
import kplay.service.giftcode.ent.GiftCodeEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author phong
 */
@Repository
public interface GiftCodeDA extends JpaRepository<GiftCodeEnt, Integer> {

    @Query(value = "SELECT * FROM kplay.giftcode WHERE campaign_id=:campaignId AND user_id=:userId", nativeQuery = true)
    public List<GiftCodeEnt> getClaimedGiftCodeByCampaign(@Param("campaignId") int campaignId, @Param("userId") int userId);

    @Query(value = "SELECT * FROM kplay.giftcode "
            + " WHERE type=:type and code=:code and user_id=0 and date_begin <=:dateNow and date_end >=:dateNow and status=1", nativeQuery = true)
    public GiftCodeEnt getAvailableGiftCode(@Param("type") int type, @Param("code") String code, @Param("dateNow") Timestamp dateNow);

    @Query(value = "SELECT * FROM kplay.giftcode "
            + " WHERE campaign_id=:campaignId", nativeQuery = true)
    public List<GiftCodeEnt> getAvailableMultipleGiftCode(@Param("campaignId") int campaignId);

}
