/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.giftcode.ent;

/**
 *
 * @author baotn
 */
public enum EGiftCodeType {
    SINGLE(0),
    MULTIPLE(1);

    private final int value;

    private EGiftCodeType(int value) {
        this.value = value;
    }

    /**
     * Get the integer value of this enum value, as defined in the Thrift IDL.
     */
    public int getValue() {
        return value;
    }

    /**
     * Find a the enum type by its integer value, as defined in the Thrift IDL.
     *
     * @return null if the value is not found.
     */
    public static EGiftCodeType findByValue(int value) {
        switch (value) {
            case 0:
                return SINGLE;
            case 1:
                return MULTIPLE;
            default:
                return null;
        }
    }
}
