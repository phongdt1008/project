/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.giftcode.ent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author baotn
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@Table(name = "kplay.giftcode")
public class GiftCodeEnt implements Serializable {

    @Id
    public int id;
    public int campaignId;
    public EGiftCodeType type;
    public String code;
    public int codeValue;
    public int userId;
    public String userName;
    public long claimTime;
    public long dateBegin;
    public long dateEnd;
    public byte status;
}
