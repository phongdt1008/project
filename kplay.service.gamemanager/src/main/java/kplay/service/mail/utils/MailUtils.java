/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.mail.utils;

import bliss.lib.framework.util.JSONUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import kplay.service.mail.ent.EMailRewardType;
import kplay.service.mail.ent.MailReward;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author baotn
 */
public class MailUtils {

    public static List<MailReward> getListRewards(String rewards) {
        List<MailReward> listReward = new ArrayList<>();

        if (StringUtils.isEmpty(rewards)) {
            return listReward;
        }

        JsonArray array = JSONUtil.DeSerialize(rewards, JsonArray.class);

        if (array == null || array.size() <= 0) {
            return listReward;
        }

        for (int i = 0; i < array.size(); i++) {
            JsonObject object = array.get(i).getAsJsonObject();

            MailReward reward = new MailReward();

            if (object.get("type").getAsString().equalsIgnoreCase("money")) {
                reward.type = EMailRewardType.MONEY.getValue();
            } else if (object.get("type").getAsString().equalsIgnoreCase("vip_point")) {
                reward.type = EMailRewardType.VIP_POINT.getValue();
            } else {
                reward.type = -1;
            }

            reward.value = object.get("value").getAsInt();

            listReward.add(reward);
        }

        return listReward;
    }
}
