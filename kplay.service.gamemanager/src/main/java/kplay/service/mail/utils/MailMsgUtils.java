/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.mail.utils;

import bliss.lib.entity.define.MailGroup;
import bliss.lib.entity.proto.BlissObjectFactory;
import bliss.lib.entity.proto.BlissObjectFactory.BlissObject;
import java.util.List;
import kplay.service.mail.ent.EMailType;
import kplay.service.mail.ent.MailEnt;
import kplay.service.mail.ent.MailReward;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author baotn
 */
public class MailMsgUtils {

    public static BlissObject.ListBlissObject.Builder addMailToBlissObject(List<MailEnt> mails) {
        BlissObjectFactory.BlissObject.ListBlissObject.Builder mailsBuilder = BlissObjectFactory.BlissObject.ListBlissObject.newBuilder();

        for (int i = 0; i < mails.size(); i++) {
            MailEnt mail = mails.get(i);
            if (mail != null) {
                BlissObject.Builder data = BlissObject.newBuilder();

                data.putMapInt(MailGroup.Key.ID_KEY, mail.id);
                data.putMapString(MailGroup.Key.TITLE_KEY, mail.title);
                data.putMapString(MailGroup.Key.CONTENT_KEY, mail.content);
                data.putMapLong(MailGroup.Key.DATE_RECEIVE_KEY, mail.receivedTime);
                data.putMapLong(MailGroup.Key.DATE_EXPIRE_KEY, mail.expiredTime);

                if (StringUtils.isEmpty(mail.rewards)) {
                    data.putMapInt(MailGroup.Key.TYPE_KEY, EMailType.NOTICE.getValue());
                } else {
                    data.putMapInt(MailGroup.Key.TYPE_KEY, EMailType.REWARD.getValue());
                }

                BlissObject.ListBlissObject.Builder rewardBuilder = addRewardToBlissObject(mail.rewards);
                data.putMapListBlissObject(MailGroup.Key.REWARDS_KEY, rewardBuilder.build());

                data.putMapInt(MailGroup.Key.STATUS_KEY, mail.status);

                mailsBuilder.addBlissVal(data);
            }
        }

        return mailsBuilder;
    }

    public static BlissObject.ListBlissObject.Builder addRewardToBlissObject(String rewards) {

        List<MailReward> mailRewards = MailUtils.getListRewards(rewards);

        BlissObjectFactory.BlissObject.ListBlissObject.Builder rewardsBuilder = BlissObjectFactory.BlissObject.ListBlissObject.newBuilder();

        if (rewards != null) {
            for (int i = 0; i < mailRewards.size(); i++) {
                BlissObject.Builder data = BlissObject.newBuilder();

                data.putMapInt(MailGroup.Key.REWARD_TYPE_KEY, mailRewards.get(i).type);
                data.putMapLong(MailGroup.Key.REWARD_VALUE_KEY, mailRewards.get(i).value);

                rewardsBuilder.addBlissVal(data);
            }
        }

        return rewardsBuilder;
    }
}
