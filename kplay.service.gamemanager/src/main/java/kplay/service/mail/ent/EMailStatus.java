/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.mail.ent;

/**
 *
 * @author baotn
 */
public enum EMailStatus {
    UNREAD(0),
    READ(1),
    DELETE(2),
    CLAIMED(3);

    private final int value;

    private EMailStatus(int value) {
        this.value = value;
    }

    /**
     * Get the integer value of this enum value, as defined in the Thrift IDL.
     */
    public int getValue() {
        return value;
    }

    /**
     * Find a the enum type by its integer value, as defined in the Thrift IDL.
     *
     * @return null if the value is not found.
     */
    public static EMailStatus findByValue(int value) {
        switch (value) {
            case 0:
                return UNREAD;
            case 1:
                return READ;
            case 2:
                return DELETE;
            case 3:
                return CLAIMED;
            default:
                return null;
        }
    }
}
