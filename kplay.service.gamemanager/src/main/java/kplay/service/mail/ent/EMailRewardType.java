/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.mail.ent;

/**
 *
 * @author baotn
 */
public enum EMailRewardType {
    MONEY(0),
    VIP_POINT(1);

    private final int value;

    private EMailRewardType(int value) {
        this.value = value;
    }

    /**
     * Get the integer value of this enum value, as defined in the Thrift IDL.
     */
    public int getValue() {
        return value;
    }

    /**
     * Find a the enum type by its integer value, as defined in the Thrift IDL.
     *
     * @return null if the value is not found.
     */
    public static EMailRewardType findByValue(int value) {
        switch (value) {
            case 0:
                return MONEY;
            case 1:
                return VIP_POINT;
            default:
                return null;
        }
    }
}
