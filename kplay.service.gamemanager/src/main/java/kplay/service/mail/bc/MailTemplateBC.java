/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.mail.bc;

import bliss.lib.framework.util.DateTimeUtils;
import java.util.List;
import kplay.service.mail.db.MailTemplateDA;
import kplay.service.mail.ent.MailTemplateEnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author baotn
 */
@Service
public class MailTemplateBC {

    @Autowired
    private MailTemplateDA mailTemplateDA;

    public MailTemplateEnt addMailTemplate(MailTemplateEnt mailTemplate) {
        mailTemplateDA.save(mailTemplate);
        return mailTemplate;
    }

    public MailTemplateEnt getMailTemplate(int id) {
        return mailTemplateDA.getOne(id);
    }

    public List<MailTemplateEnt> getCurrentListMailTemplate(long date, int status) {
        return mailTemplateDA.getCurrentListMailTemplate(DateTimeUtils.getTimestampSql(date), status);
    }

    public boolean updateMailTemplate(MailTemplateEnt mailTemplate) {
        try {
            MailTemplateEnt ent = mailTemplateDA.getOne(mailTemplate.getId());

            ent.setContent(mailTemplate.getContent());
            ent.setExpiredTime(mailTemplate.getExpiredTime());
            ent.setReceivedTime(mailTemplate.getReceivedTime());
            ent.setRewards(mailTemplate.getRewards());
            ent.setStatus(mailTemplate.getStatus());
            ent.setTitle(mailTemplate.getTitle());

            mailTemplateDA.save(ent);
            return true;
        } catch (Exception e) {
        }
        return false;
    }
}
