/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.mail.bc;

import bliss.lib.entity.define.MailGroup;
import bliss.lib.entity.proto.BlissObjectFactory;
import bliss.lib.entity.proto.BlissObjectFactory.BlissObject;
import bliss.lib.entity.thrift.TUserMoney;
import bliss.lib.entity.utils.BlissMessageUtils;
import bliss.lib.framework.thriftgen.tcommon.TSession;
import bliss.lib.framework.util.DateTimeUtils;
import java.util.ArrayList;
import java.util.List;
import kplay.service.gamemanager.MsgHelper;
import kplay.service.gamemanager.adapter.ConnectorAdapter;
import kplay.service.gamemanager.adapter.PlayerAdapter;
import kplay.service.mail.db.MailDA;
import kplay.service.mail.ent.EMailRewardType;
import kplay.service.mail.ent.EMailStatus;
import kplay.service.mail.ent.MailEnt;
import kplay.service.mail.ent.MailReward;
import kplay.service.mail.utils.MailMsgUtils;
import kplay.service.mail.utils.MailUtils;
import kplay.thrift.player.TUpdateMoneyResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author baotn
 */
@Service
public class MailBC {

    @Autowired
    private MailDA mailDA;

    public MailEnt addMail(MailEnt mail) {
        mailDA.save(mail);
        return mail;
    }

    public MailEnt getMail(int id) {
        return mailDA.getOne(id);
    }

    public List<MailEnt> getCurrentListMail(int userId) {
        return mailDA.getCurrentListMail(userId, EMailStatus.DELETE.getValue(), EMailStatus.CLAIMED.getValue());
    }

    public boolean updateMail(MailEnt mail) {
        try {
            MailEnt ent = mailDA.getOne(mail.getId());

            ent.setContent(mail.getContent());
            ent.setExpiredTime(mail.getExpiredTime());
            ent.setReceivedTime(mail.getReceivedTime());
            ent.setRewards(mail.getRewards());
            ent.setStatus(mail.getStatus());
            ent.setTemplateId(mail.getTemplateId());
            ent.setTitle(mail.getTitle());
            ent.setUserId(mail.getUserId());

            mailDA.save(ent);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public void getCurrentListMail(int userId, TSession session, BlissObjectFactory.BlissObject data) {
        List<MailEnt> listMail = getCurrentListMail(userId);

        BlissObject.Builder dataResponse = BlissObject.newBuilder();

        BlissObject.ListBlissObject.Builder mailBuilder = MailMsgUtils.addMailToBlissObject(listMail);

        dataResponse.putMapListBlissObject(MailGroup.Key.LIST_MAIL_KEY, mailBuilder.build());

        ConnectorAdapter.sendMsgToUserId(userId, BlissMessageUtils.getBlissMessage(MailGroup.CMD.GET_LIST_MAIL_CMD, dataResponse.build()));
    }

    public void readMail(int userId, TSession session, BlissObjectFactory.BlissObject data) {
        int id = data.getMapIntMap().get(MailGroup.Key.ID_KEY);

        if (id > 0) {
            MailEnt mail = getMail(id);
            mail.status = EMailStatus.READ.getValue();
            updateMail(mail);

            BlissObject.Builder dataResponse = BlissObject.newBuilder();

            List<MailEnt> listMail = new ArrayList<>();
            listMail.add(mail);

            BlissObject.ListBlissObject.Builder mailBuilder = MailMsgUtils.addMailToBlissObject(listMail);

            dataResponse.putMapListBlissObject(MailGroup.Key.LIST_MAIL_KEY, mailBuilder.build());

            ConnectorAdapter.sendMsgToUserId(userId, BlissMessageUtils.getBlissMessage(MailGroup.CMD.READ_MAIL_CMD, dataResponse.build()));
        } else {
            List<MailEnt> listMail = getCurrentListMail(userId);
            List<MailEnt> listResultMail = new ArrayList<>();

            for (int i = 0; i < listMail.size(); i++) {
                MailEnt mail = listMail.get(i);

                if (mail.status == EMailStatus.UNREAD.getValue()) {
                    mail.status = EMailStatus.READ.getValue();
                    updateMail(mail);
                    listResultMail.add(getMail(mail.id));
                }
            }

            BlissObject.Builder dataResponse = BlissObject.newBuilder();

            BlissObject.ListBlissObject.Builder mailBuilder = MailMsgUtils.addMailToBlissObject(listResultMail);

            dataResponse.putMapListBlissObject(MailGroup.Key.LIST_MAIL_KEY, mailBuilder.build());

            ConnectorAdapter.sendMsgToUserId(userId, BlissMessageUtils.getBlissMessage(MailGroup.CMD.READ_MAIL_CMD, dataResponse.build()));
        }
    }

    public void deleteMail(int userId, TSession session, BlissObjectFactory.BlissObject data) {
        int id = data.getMapIntMap().get(MailGroup.Key.ID_KEY);

        if (id > 0) {
            MailEnt mail = getMail(id);

            if (!StringUtils.isEmpty(mail.rewards) && mail.status != EMailStatus.CLAIMED.getValue() && mail.status != EMailStatus.DELETE.getValue() && (mail.expiredTime - DateTimeUtils.getMilisecondsNow()) >= 0) {
                return;
            }

            mail.status = EMailStatus.DELETE.getValue();
            updateMail(mail);

            BlissObject.Builder dataResponse = BlissObject.newBuilder();

            List<MailEnt> listMail = new ArrayList<>();
            listMail.add(mail);

            BlissObject.ListBlissObject.Builder mailBuilder = MailMsgUtils.addMailToBlissObject(listMail);

            dataResponse.putMapListBlissObject(MailGroup.Key.LIST_MAIL_KEY, mailBuilder.build());

            ConnectorAdapter.sendMsgToUserId(userId, BlissMessageUtils.getBlissMessage(MailGroup.CMD.DELETE_MAIL_CMD, dataResponse.build()));
        } else {
            List<MailEnt> listMail = getCurrentListMail(userId);
            List<MailEnt> listResultMail = new ArrayList<>();

            for (int i = 0; i < listMail.size(); i++) {
                MailEnt mail = listMail.get(i);

                if (!StringUtils.isEmpty(mail.rewards) && mail.status != EMailStatus.CLAIMED.getValue() && mail.status != EMailStatus.DELETE.getValue() && (mail.expiredTime - DateTimeUtils.getMilisecondsNow()) >= 0) {
                    continue;
                }

                mail.status = EMailStatus.DELETE.getValue();
                updateMail(mail);
                listResultMail.add(getMail(mail.id));
            }

            BlissObject.Builder dataResponse = BlissObject.newBuilder();

            BlissObject.ListBlissObject.Builder mailBuilder = MailMsgUtils.addMailToBlissObject(listResultMail);

            dataResponse.putMapListBlissObject(MailGroup.Key.LIST_MAIL_KEY, mailBuilder.build());

            ConnectorAdapter.sendMsgToUserId(userId, BlissMessageUtils.getBlissMessage(MailGroup.CMD.DELETE_MAIL_CMD, dataResponse.build()));
        }
    }

    public void claimMail(int userId, TSession session, BlissObjectFactory.BlissObject data) {
        int id = data.getMapIntMap().get(MailGroup.Key.ID_KEY);

        if (id > 0) {
            MailEnt mail = getMail(id);

            if (!StringUtils.isEmpty(mail.rewards) && mail.status != EMailStatus.CLAIMED.getValue() && mail.status != EMailStatus.DELETE.getValue() && (mail.expiredTime - DateTimeUtils.getMilisecondsNow()) >= 0) {
                mail.status = EMailStatus.CLAIMED.getValue();
                updateMail(mail);
                mail = getMail(id);
                long goldReward = getGoldReward(mail.rewards);
                TUpdateMoneyResponse response = PlayerAdapter.addMoney(new TUserMoney(session.getSessionID(), session.getUserID(), session.getDisplayName()), goldReward, mail);

                if (goldReward > 0) {
                    sendPopupReward(session, goldReward, response, "POPUP_BODY_CONGRATULATION_PURCHASE_SUCCESS");
                }

                BlissObject.Builder dataResponse = BlissObject.newBuilder();

                List<MailEnt> listMail = new ArrayList<>();
                listMail.add(mail);

                BlissObject.ListBlissObject.Builder mailBuilder = MailMsgUtils.addMailToBlissObject(listMail);

                dataResponse.putMapListBlissObject(MailGroup.Key.LIST_MAIL_KEY, mailBuilder.build());

                ConnectorAdapter.sendMsgToUserId(userId, BlissMessageUtils.getBlissMessage(MailGroup.CMD.CLAIM_MAIL_CMD, dataResponse.build()));
            }
        } else {
            List<MailEnt> listMail = getCurrentListMail(userId);
            List<MailEnt> listResultMail = new ArrayList<>();

            TUpdateMoneyResponse response = new TUpdateMoneyResponse();
            long goldReward = 0;
            for (int i = 0; i < listMail.size(); i++) {
                MailEnt mail = listMail.get(i);

                if (!StringUtils.isEmpty(mail.rewards) && mail.status != EMailStatus.CLAIMED.getValue() && mail.status != EMailStatus.DELETE.getValue() && (mail.expiredTime - DateTimeUtils.getMilisecondsNow()) >= 0) {
                    mail.status = EMailStatus.CLAIMED.getValue();
                    updateMail(mail);
                    goldReward += getGoldReward(listMail.get(i).rewards);
                    response = PlayerAdapter.addMoney(new TUserMoney(session.getSessionID(), session.getUserID(), session.getDisplayName()), getGoldReward(listMail.get(i).rewards), listMail.get(i));
                    listResultMail.add(getMail(mail.id));
                }
            }

            if (goldReward > 0) {
                sendPopupReward(session, goldReward, response, "POPUP_BODY_CONGRATULATION_PURCHASE_SUCCESS");
            }

            BlissObject.Builder dataResponse = BlissObject.newBuilder();

            BlissObject.ListBlissObject.Builder mailBuilder = MailMsgUtils.addMailToBlissObject(listResultMail);

            dataResponse.putMapListBlissObject(MailGroup.Key.LIST_MAIL_KEY, mailBuilder.build());

            ConnectorAdapter.sendMsgToUserId(userId, BlissMessageUtils.getBlissMessage(MailGroup.CMD.CLAIM_MAIL_CMD, dataResponse.build()));
        }
    }

    private static void sendPopupReward(TSession session, long goldReward, TUpdateMoneyResponse response, String content) {
        ConnectorAdapter.sendMsgToUserId(session.getUserID(), MsgHelper.getUpdatingMoneyPopupMessage(goldReward, response.getBeforeUpdate(), response.getAfterUpdate(), content, session.getDisplayName()));
    }

    public static long getGoldReward(String rewards) {

        List<MailReward> listRewards = MailUtils.getListRewards(rewards);

        long gold = 0;
        for (int i = 0; i < listRewards.size(); i++) {
            if (listRewards.get(i).type == EMailRewardType.MONEY.getValue()) {
                gold += listRewards.get(i).value;
            }
        }

        return gold;
    }

}
