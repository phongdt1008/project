/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.mail.db;

import java.sql.Timestamp;
import java.util.List;
import kplay.service.mail.ent.MailTemplateEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author baotn
 */
@Repository
public interface MailTemplateDA extends JpaRepository<MailTemplateEnt, Integer> {

    @Query(value = "SELECT * FROM kplay.mail_template where received_time <= :date and expired_time >= :date and status = :status order by received_time asc", nativeQuery = true)
    public List<MailTemplateEnt> getCurrentListMailTemplate(@Param(value = "date") Timestamp date, @Param(value = "status") int status);
}
