/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.mail.db;

import java.util.List;
import kplay.service.mail.ent.EMailStatus;
import kplay.service.mail.ent.MailEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author baotn
 */
@Repository
public interface MailDA extends JpaRepository<MailEnt, Integer> {

    @Query(value = "SELECT * FROM kplay.mail where user_id = :userId and status != :statusDelete and status != :statusClaimed order by received_time asc", nativeQuery = true)
    public List<MailEnt> getCurrentListMail(@Param(value = "userId") int userId,
            @Param(value = "statusDelete") int statusDelete,
            @Param(value = "statusClaimed") int statusClaimed);
}
