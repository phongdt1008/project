/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.gamemanager;

import bliss.lib.entity.define.EventGroup;
import bliss.lib.entity.define.MailGroup;
import kplay.service.gamemanager.common.business.GameManagerBC;
import bliss.lib.framework.common.LogUtil;
import bliss.lib.framework.thriftgen.moduleservice.LogoutReason;
import bliss.lib.framework.thriftgen.moduleservice.LogoutStatus;
import bliss.lib.framework.thriftgen.tcommon.AdminCommand;
import bliss.lib.framework.thriftgen.tcommon.ServiceStatus;
import bliss.lib.framework.thriftgen.tcommon.TAdminRequest;
import bliss.lib.framework.thriftgen.tcommon.TInternalResponse;
import bliss.lib.framework.thriftgen.tcommon.TResponseInfo;
import bliss.lib.framework.thriftgen.tcommon.TSession;
import ga.log4j.GA;
import java.nio.ByteBuffer;
import bliss.lib.entity.proto.BlissMessageFactory;
import bliss.lib.entity.proto.BlissObjectFactory;
import bliss.lib.framework.thriftgen.moduleservice.TModuleService;
import bliss.lib.framework.util.JSONUtil;
import com.google.protobuf.InvalidProtocolBufferException;
import kplay.service.event.bc.EventBC;
import kplay.service.gamemanager.common.entity.TBroadcast;
import kplay.service.mail.bc.MailBC;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author phong
 */
@Component
public class TGameManagerProcessor implements TModuleService.Iface {

    @Autowired
    private EventBC eventBC;
    @Autowired
    private MailBC mailBC;

    @Override
    public TResponseInfo pushMessage(String token, int moduleid, TSession session, int msgType, int length, ByteBuffer msgData) throws TException {

        try {
            BlissMessageFactory.BlissMessage message = BlissMessageFactory.BlissMessage.parseFrom(msgData);
//            if (GameManagerBC.hasNewVersion(session) && new Version(session.getVersion()).compareTo(new Version("1.10.3")) >= 0) {
//                GameManagerBC.kickOutHasNewVersion(session);
//            } else {
            processMessage(session, message);
//            }
            return new TResponseInfo();
        } catch (InvalidProtocolBufferException ex) {
            GA.app.error(LogUtil.stackTrace(ex));

            return new TResponseInfo();
        }
    }

    @Override
    public TInternalResponse internalCall(String token, int moduleid, int userid, int msgType, int length, ByteBuffer msgData) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LogoutStatus logout(String token, int userid, int moduleid, int roomid, LogoutReason reason) {
        System.out.println("Receive Logout");
        return LogoutStatus.SUCCESS;
    }

    @Override
    public void reconnect(String token, int userid, String params) throws TException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TResponseInfo perform(AdminCommand cmd, String token, TAdminRequest data) throws TException {

        TBroadcast broadcast = JSONUtil.DeSerialize(new String(data.getMsgData()), TBroadcast.class);

        switch (broadcast.getAction()) {
            case ENABLE:
                GameManagerBC.enableBroadcast(broadcast);
                break;
            case DISABLE:
                System.out.println("Disable Broadcast " + broadcast.toString());
                GameManagerBC.disableBroadcast();
                break;
            default:
                return null;
        }
        return new TResponseInfo();
    }

    @Override
    public ServiceStatus getStatus() throws TException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean ping() throws TException {
        return true;
    }

//    public static void main(String[] args) {
//        try {
//            TBroadcast params = new TBroadcast();
//            params.setAction(BroadcastAction.ENABLE);
//            params.setContent("Mọi người ơi, mọi người ơi, sắp bảo trì rồi mọi người ơi");
//            params.setPeriodTime(10 * 1000);
//            params.setDurationTime(1 * 24 * 60 * 60 * 1000);
//
//            TSerializer encoder = new TSerializer();
//            byte[] data = encoder.serialize(params);
//            TProfile object = new TProfile();
//            TDeserializer decoder = new TDeserializer();
//            decoder.deserialize(object, data);
//            System.out.println("Deserializer ");
//        } catch (TException ex) {
//            System.out.println("Log exception: " + ex.getMessage());
//            Logger.getLogger(TGameManagerProcessor.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    private void processMessage(TSession session, BlissMessageFactory.BlissMessage message) {
        try {
            int userId = session.userID;
            int command = message.getCmd();
            BlissObjectFactory.BlissObject data = message.getData();
            GA.app.error("process command : " + command);
            switch (command) {
                //email group
                case MailGroup.CMD.GET_LIST_MAIL_CMD:
                    mailBC.getCurrentListMail(userId, session, data);
                    break;
                case MailGroup.CMD.READ_MAIL_CMD:
                    mailBC.readMail(userId, session, data);
                    break;
                case MailGroup.CMD.DELETE_MAIL_CMD:
                    mailBC.deleteMail(userId, session, data);
                    break;
                case MailGroup.CMD.CLAIM_MAIL_CMD:
                    mailBC.claimMail(userId, session, data);
                    break;

                //event group
                case EventGroup.CMD.GET_CURRENT_LIST_EVENT_CMD:
                    eventBC.getCurrentListEvent(userId, session, data);
                    break;
            }
        } catch (Exception ex) {
            GA.app.error(LogUtil.stackTrace(ex));
        }
    }
}
