/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.gamemanager.common.business;

import bliss.lib.entity.define.SystemGroup;
import bliss.lib.entity.encode.BPObjectEncode;
import bliss.lib.entity.ent.Version;
import kplay.service.gamemanager.common.entity.BroadcastEntity;
import kplay.service.gamemanager.MsgHelper;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import bliss.lib.entity.utils.BlissMessageUtils;
import bliss.lib.framework.common.LogUtil;
import bliss.lib.framework.thriftgen.tcommon.TSession;
import ga.log4j.GA;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import kplay.service.gamemanager.adapter.ConnectorAdapter;
import kplay.service.gamemanager.common.entity.TBroadcast;
import kplay.service.gamemanager.common.redis.VersionControlCA;

/**
 *
 * @author phong
 */
public class GameManagerBC {

    private static BroadcastEntity broadcast = new BroadcastEntity();

    private static ScheduledThreadPoolExecutor schedules = null;

    public static void enableBroadcast(TBroadcast params) {
        System.out.println("Broadcast params: " + params.toString());
        broadcast.setContent(params.getContent());
        broadcast.setActive(true);
        broadcast.setPeriodTime(params.getPeriodTime());
        broadcast.setEndTime(System.currentTimeMillis() + params.getDurationTime());

        schedules = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1);

        schedules.scheduleWithFixedDelay(() -> {
            try {
                updateBroadcast();
            } catch (Exception ex) {
                GA.app.error(LogUtil.stackTrace(ex));
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

    public static void disableBroadcast() {
        schedules.shutdown();
    }

    private static void updateBroadcast() {
        if (System.currentTimeMillis() >= broadcast.getEndTime()) {
            disableBroadcast();
            return;
        }

        if (System.currentTimeMillis() - broadcast.getLastBroadcastTime() >= broadcast.getPeriodTime()) {
            ConnectorAdapter.sendAllPlayer(MsgHelper.getBroacastMessage(broadcast.getContent()));

            broadcast.setLastBroadcastTime(System.currentTimeMillis());
        }
    }

//    public static boolean hasNewVersion(TSession session) {
//        return new Version(session.getVersion()).compareTo(VersionControlCA.getVersionStore()) < 0
//                || (new Version(session.getVersion()).compareTo(VersionControlCA.getNewestVersion()) < 0 && VersionControlCA.isForced());
//    }
    public static void kickOutHasNewVersion(TSession session) {
        ConnectorAdapter.sendMsgToUserId(session.getUserID(), BlissMessageUtils.getBlissMessage(SystemGroup.CMD.FORCE_VERSION, new BPObjectEncode()));
    }
}
