/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.gamemanager.common.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

/**
 *
 * @author phong
 */
@Accessors(chain = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BroadcastEntity {

    String content;
    boolean isActive;
    long periodTime; // ms
    long lastBroadcastTime; //ms
    long endTime; //ms

}
