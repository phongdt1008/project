/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.gamemanager.common.redis;

import bliss.lib.entity.ent.Version;
import bliss.lib.framework.redis.RedisFactory;
import kplay.service.gamemanager.config.ModuleConfig;
import org.apache.commons.lang.StringUtils;
import redis.clients.jedis.Jedis;

/**
 *
 * @author khoatd
 */
public class VersionControlCA {

    public static final String VERSION_NEWEST = "dlc:version_newest";
    public static final String VERSION_CURRENT = "dlc:version_current";
    public static final String VERSION_STORE = "dlc:version_store";
    public static final String FORCE = "dlc:version_force";

    Jedis jedis = null;
    RedisFactory client = null;

    public static Version getVersionStore() {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();

            String data = jedis.get(VERSION_STORE);
            if (StringUtils.isEmpty(data)) {
                return null;
            }

            return new Version(data);
        } catch (Exception ex) {
            return null;
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
    }

    public static Version getNewestVersion() {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();

            String data = jedis.get(VERSION_NEWEST);
            if (StringUtils.isEmpty(data)) {
                return null;
            }

            return new Version(data);
        } catch (Exception ex) {
            return null;
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
    }

    public static boolean isForced() {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();

            String data = jedis.get(FORCE);
            if (StringUtils.isEmpty(data)) {
                return false;
            }

            return Integer.valueOf(data) > 0;
        } catch (Exception ex) {
            return false;
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
    }
}
