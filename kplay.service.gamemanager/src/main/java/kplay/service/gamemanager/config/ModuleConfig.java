/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.gamemanager.config;

import bliss.lib.framework.common.Config;

/**
 *
 * @author baotn
 */
public class ModuleConfig {

    public static final String SERVICE_CONFIG = "service_config";

    public static final String SPRING_BOOT_CONFIG = Config.getParam("spring_boot_config", "conf_path");

    public static final String REDIS_CONFIG = "redis_config";
    public static final String DB_CONFIG = "db_config";
    public static final String CONNECTOR_CONFIG = "connector_config";
    public static final String WORKER_CONFIG = "worker_config";
    public static final String PLAYER_CONFIG = "player_config";

    public static final String WORKER_PAYMENT_CONFIG = "worker_payment_config";
    public static final String IAP_CONFIG = "iap_config";
}
