/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.gamemanager.adapter;

import bliss.lib.entity.proto.BlissMessageFactory;
import bliss.lib.entity.utils.BlissMessageUtils;
import bliss.lib.framework.thriftgen.tcommon.TResponseInfo;
import bliss.lib.framework.thriftmodule.connector.client.TConnectorServiceClient;
import java.util.ArrayList;
import java.util.List;
import kplay.service.gamemanager.config.ModuleConfig;

/**
 *
 * @author Huy Nguyen
 */
public class ConnectorAdapter {

    public static void sendMsgToUserId(int userIdReceive, BlissMessageFactory.BlissMessage... m) {
        if (m.length == 1) {
            TResponseInfo response = BlissMessageUtils.getResponseInfo(userIdReceive, m[0]);
            TConnectorServiceClient.getInstance(ModuleConfig.CONNECTOR_CONFIG).pushResponse(response);
        } else {
            List<TResponseInfo> lst = new ArrayList<>();
            for (BlissMessageFactory.BlissMessage m1 : m) {
                lst.add(BlissMessageUtils.getResponseInfo(userIdReceive, m1));
            }
            TConnectorServiceClient.getInstance(ModuleConfig.CONNECTOR_CONFIG).pushResponses(lst);
        }
    }
    
    public static void sendAllPlayer(BlissMessageFactory.BlissMessage msg) {
        List<Integer> listUserId = null;
        TResponseInfo response = BlissMessageUtils.getResponseInfo(listUserId, msg);
        response.setUserIdReceive(null);
        TConnectorServiceClient.getInstance(ModuleConfig.CONNECTOR_CONFIG).pushResponse(response);
    }
    
    public static void sendToListPlayer(BlissMessageFactory.BlissMessage msg, List<Integer> listUserId) {
        TResponseInfo response = BlissMessageUtils.getResponseInfo(listUserId, msg);
        TConnectorServiceClient.getInstance(ModuleConfig.CONNECTOR_CONFIG).pushResponse(response);
    }

}
