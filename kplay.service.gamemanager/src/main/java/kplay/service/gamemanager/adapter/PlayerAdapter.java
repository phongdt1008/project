/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.gamemanager.adapter;

import bliss.lib.entity.thrift.TRefType;
import bliss.lib.entity.thrift.TUserMoney;
import bliss.lib.framework.thriftgen.tcommon.TSession;
import java.util.List;
import kplay.service.gamemanager.config.ModuleConfig;
import kplay.service.mail.ent.MailEnt;
import kplay.thrift.player.TUpdateMoneyResponse;
import kplay.thrift.player.client.TPlayerClient;

/**
 *
 * @author baotn
 */
public class PlayerAdapter {

    private static final TPlayerClient tPlayer;

    static {
        tPlayer = TPlayerClient.getInstance(ModuleConfig.PLAYER_CONFIG);
    }

    public static TSession getByUserID(int userId) {
//        return BlissSession.getByUserID(userId);
        return tPlayer.getSession(userId);
    }

    public static List<TSession> getUserOnline(int size) {
//        return BlissSession.getUserOnline(size);
        return tPlayer.getUserOnline(size);
    }

    public static TUpdateMoneyResponse addMoney(TUserMoney user, long money, TRefType refType, int refID) {
        return tPlayer.addMoney(user, money, refType, refID);
    }

    public static TUpdateMoneyResponse addMoney(TUserMoney user, long money, MailEnt mail) {
        return tPlayer.addMoney(user, money, TRefType.SYSTEM, mail.id);
    }

    public static TUpdateMoneyResponse substractMoney(TUserMoney user, long money, TRefType refType, int refID) {
        return tPlayer.subtractMoney(user, money, refType, refID);
    }

}
