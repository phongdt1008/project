/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.gamemanager;

import bliss.lib.entity.define.SystemGroup;
import bliss.lib.entity.encode.BPObjectEncode;
import bliss.lib.entity.proto.BlissMessageFactory;
import bliss.lib.entity.thrift.TPopupCode;

/**
 *
 * @author khoatd
 */
public class MsgHelper {

    public static BlissMessageFactory.BlissMessage getBroacastMessage(String content) {
        BPObjectEncode encoder = new BPObjectEncode();
        encoder.putString(SystemGroup.Key.BROADCAST_CONTENT_KEY, content);
        return getBlissMessage(SystemGroup.CMD.BROADCAST_CMD, encoder);
    }

    public static BlissMessageFactory.BlissMessage getGenericPopupMessage(TPopupCode popupCode, String title, String content) {
        BPObjectEncode data = new BPObjectEncode();
        data.putInt32(SystemGroup.Key.POPUP_CODE_KEY, popupCode.getValue());
        data.putString(SystemGroup.Key.POPUP_TITLE_KEY, title);
        data.putString(SystemGroup.Key.POPUP_CONTENT_KEY, content);

        return getBlissMessage(SystemGroup.CMD.POPUP_CMD, data);
    }

    public static BlissMessageFactory.BlissMessage getUpdatingMoneyPopupMessage(long changedValue, long before, long after, String reason) {
        BPObjectEncode data = new BPObjectEncode();
        data.putInt64(SystemGroup.Key.CHANGED_VALUE_KEY, changedValue);
        data.putInt64(SystemGroup.Key.BEFORE_UPDATE_MONEY_KEY, before);
        data.putInt64(SystemGroup.Key.AFTER_UPDATE_MONEY_KEY, after);
        data.putString(SystemGroup.Key.REASON_UPDATE_MONEY_KEY, reason);

        return getBlissMessage(SystemGroup.CMD.UPDATE_MONEY_POPUP_CMD, data);
    }

    public static BlissMessageFactory.BlissMessage getUpdatingMoneyPopupMessage(long changedValue, long before, long after, String reason, String valueContent) {
        BPObjectEncode data = new BPObjectEncode();
        data.putInt64(SystemGroup.Key.CHANGED_VALUE_KEY, changedValue);
        data.putInt64(SystemGroup.Key.BEFORE_UPDATE_MONEY_KEY, before);
        data.putInt64(SystemGroup.Key.AFTER_UPDATE_MONEY_KEY, after);
        data.putString(SystemGroup.Key.REASON_UPDATE_MONEY_KEY, reason);
        data.putBoolen(SystemGroup.Key.IS_SHOW_UPDATE_MONEY_KEY, true);
        data.putString(SystemGroup.Key.CONTENT_EXTEND_KEY, valueContent);

        return getBlissMessage(SystemGroup.CMD.UPDATE_MONEY_POPUP_CMD, data);
    }

    public static BlissMessageFactory.BlissMessage getBlissMessage(int cmd, BPObjectEncode data) {
        BlissMessageFactory.BlissMessage.Builder messageBuilder = BlissMessageFactory.BlissMessage.newBuilder();

        messageBuilder.setCmd(cmd);
        messageBuilder.setData(data.getBlissObject());

        return messageBuilder.build();
    }
}
