/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.gamemanager;

import bliss.lib.framework.thriftgen.moduleservice.TModuleService;
import bliss.lib.framework.thriftutil.TServerManager;
import ga.log4j.GA;
import java.net.InetSocketAddress;
import kplay.service.gamemanager.config.ModuleConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author phong
 */
@Component
public class TServer {

    @Autowired
    private TGameManagerProcessor processor;

    public void runServer() {
        try {
            TProcessor tProcessor = new TModuleService.Processor<>(processor);
            TServerManager server = new TServerManager();
            System.err.println("GameManager server start .... ");
            server.start(ModuleConfig.SERVICE_CONFIG, tProcessor);
        } catch (Exception e) {
            GA.app.error("Don't start Server Thrift " + e.getMessage(), e);
            System.exit(1);
        }
    }
//
//    public void runThreadPool() throws TTransportException {
//        String host = null;
//        if (host == null || host.isEmpty()) {
//            host = "0.0.0.0";
//        }
//        Integer port = 1111;
//        Integer minThread = 256;
//        Integer maxThread = 1024;
//
//        InetSocketAddress inetSock = new InetSocketAddress(host, port);
//        TServerTransport serverTransport = new TServerSocket(inetSock);
//        TProcessor tProcessor = new TModuleService.Processor<>(processor);
//        TThreadPoolServer.Args args2 = new TThreadPoolServer.Args(serverTransport).processor(tProcessor);
//        args2.transportFactory(new TFramedTransport.Factory());
//        args2.protocolFactory(new TBinaryProtocol.Factory());
//        args2.minWorkerThreads(minThread);
//        args2.maxWorkerThreads(maxThread);
//        org.apache.thrift.server.TServer threadPoolServer = new TThreadPoolServer(args2);
//        GA.app.info("GameManager server start .... ");
//        threadPoolServer.serve();
//    }
}
