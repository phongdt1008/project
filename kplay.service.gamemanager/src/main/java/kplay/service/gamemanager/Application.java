package kplay.service.gamemanager;

import kplay.service.event.bc.EventBC;
import kplay.service.event.ent.EventEnt;
import kplay.service.gamemanager.config.ModuleConfig;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "kplay.service")
@EnableJpaAuditing
@EntityScan(basePackages = "kplay.service")
@EnableJpaRepositories("kplay.service")
public class Application {

    public static void main(String[] args) {
        System.err.println("==============>" + ModuleConfig.SPRING_BOOT_CONFIG);
        ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(Application.class)
                .properties("spring.config.location:" + ModuleConfig.SPRING_BOOT_CONFIG)
                .build().run(args);
        final EventBC eventBC = applicationContext.getBean(EventBC.class);

        EventEnt ent = new EventEnt();
        ent.setTitle("ok nha");

        eventBC.saveEvent(ent);
        final TServer server = applicationContext.getBean(TServer.class);
        server.runServer();
    }

}
