/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.trigger.db;

import java.sql.Timestamp;
import java.util.List;
import kplay.service.trigger.ent.TriggerEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author baotn
 */
@Repository
public interface TriggerDA extends JpaRepository<TriggerEnt, Integer> {

    @Query(value = "SELECT * FROM kplay.trigger where start_time <= :now and end_time >= :now and status = :status", nativeQuery = true)
    public List<TriggerEnt> getCurrentListTrigger(@Param(value = "now") Timestamp now, @Param(value = "status") int status);

    @Query(value = "SELECT * FROM kplay.trigger where type = :type and ref_id = :refId", nativeQuery = true)
    public TriggerEnt getTrigger(@Param(value = "type") int type, @Param(value = "refId") int refId);

    @Query(value = "SELECT * FROM kplay.trigger where type = :type and start_time <= :now and end_time >= :now and status = :status", nativeQuery = true)
    public List<TriggerEnt> getCurrentListTrigger(@Param(value = "type") int type, @Param(value = "now") Timestamp now, @Param(value = "status") int status);
}
