/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.trigger.bc;

import bliss.lib.framework.common.LogUtil;
import bliss.lib.framework.util.DateTimeUtils;
import bliss.lib.framework.util.JSONUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import ga.log4j.GA;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import kplay.service.trigger.db.TriggerDA;
import kplay.service.trigger.ent.TriggerEnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author baotn
 */
@Service
public class TriggerBC {

    @Autowired
    private TriggerDA triggerDA;

    public TriggerEnt addTrigger(TriggerEnt triggerEnt) {
        triggerDA.save(triggerEnt);
        return triggerEnt;
    }

    public boolean updateTrigger(TriggerEnt triggerEnt) {
        try {
            TriggerEnt dbEnt = triggerDA.getOne(triggerEnt.getId());
            dbEnt.setEndTime(triggerEnt.getEndTime());
            dbEnt.setRefId(triggerEnt.getRefId());
            dbEnt.setStartTime(triggerEnt.getStartTime());
            dbEnt.setStatus(triggerEnt.getStatus());
            dbEnt.setTriggerTime(triggerEnt.getTriggerTime());
            dbEnt.setType(triggerEnt.getType());
            triggerDA.save(dbEnt);
            return true;
        } catch (Exception e) {
            GA.app.error(LogUtil.stackTrace(e));
        }
        return true;
    }

    public List<TriggerEnt> getCurrentListTrigger(long now, int status) {

        List<TriggerEnt> listTrigger = triggerDA.getCurrentListTrigger(DateTimeUtils.getTimestampSql(now), status);

        List<TriggerEnt> currentListTrigger = new ArrayList();

        for (int i = 0; i < listTrigger.size(); i++) {
            if (checkAvailableTrigger(listTrigger.get(i))) {
                currentListTrigger.add(listTrigger.get(i));
            }
        }

        return currentListTrigger;
    }

    public List<TriggerEnt> getCurrentListTrigger(int type, long now, int status) {

        List<TriggerEnt> listTrigger = triggerDA.getCurrentListTrigger(type, DateTimeUtils.getTimestampSql(now), status);

        List<TriggerEnt> currentListTrigger = new ArrayList();

        for (int i = 0; i < listTrigger.size(); i++) {
            if (checkAvailableTrigger(listTrigger.get(i))) {
                currentListTrigger.add(listTrigger.get(i));
            }
        }

        return currentListTrigger;
    }

    private boolean checkAvailableTrigger(TriggerEnt triggerEnt) {
        JsonObject object = JSONUtil.DeSerialize(triggerEnt.triggerTime, JsonObject.class);

        JsonArray active_times = object.get("active_times").getAsJsonArray();

        if (active_times.size() <= 0) {
            return false;
        }

        int timeInMins = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) * 60 + Calendar.getInstance().get(Calendar.MINUTE);
        boolean available = false;
        for (int i = 0; i < active_times.size(); i++) {
            JsonArray minutes = active_times.get(i).getAsJsonArray();
            if (minutes.get(0).getAsInt() <= timeInMins && timeInMins <= minutes.get(1).getAsInt()) {
                available = true;
                break;
            }
        }

        if (available == false) {
            return false;
        }

        JsonArray active_days = object.get("active_days").getAsJsonArray();
        JsonArray repeat_days_in_week = object.get("repeat_days_in_week").getAsJsonArray();
        JsonArray repeat_days_in_month = object.get("repeat_days_in_month").getAsJsonArray();

        if (active_days.size() <= 0 && repeat_days_in_week.size() <= 0 && repeat_days_in_month.size() <= 0) {
            return false;
        }

        int dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        if (active_days.size() > 0) {
            for (int i = 0; i < active_days.size(); i++) {
                if (dayOfMonth == active_days.get(i).getAsInt()) {
                    return true;
                }
            }
        }

        if (repeat_days_in_month.size() > 0) {
            for (int i = 0; i < repeat_days_in_month.size(); i++) {
                if (dayOfMonth == repeat_days_in_month.get(i).getAsInt()) {
                    return true;
                }
            }
        }

        int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

        if (repeat_days_in_week.size() > 0) {
            for (int i = 0; i < repeat_days_in_week.size(); i++) {
                if (dayOfWeek == repeat_days_in_week.get(i).getAsInt()) {
                    return true;
                }
            }
        }

        return false;
    }
}
