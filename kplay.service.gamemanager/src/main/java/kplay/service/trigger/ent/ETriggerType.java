/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.trigger.ent;

/**
 *
 * @author baotn
 */
public enum ETriggerType {
    MAIL(0),
    EVENT(1);

    private final int value;

    private ETriggerType(int value) {
        this.value = value;
    }

    /**
     * Get the integer value of this enum value, as defined in the Thrift IDL.
     */
    public int getValue() {
        return value;
    }

    /**
     * Find a the enum type by its integer value, as defined in the Thrift IDL.
     *
     * @return null if the value is not found.
     */
    public static ETriggerType findByValue(int value) {
        switch (value) {
            case 0:
                return MAIL;
            case 1:
                return EVENT;
            default:
                return null;
        }
    }
}
