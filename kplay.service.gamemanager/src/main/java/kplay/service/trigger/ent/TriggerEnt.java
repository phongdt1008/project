/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.trigger.ent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author baotn
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@Table(name = "kplay.trigger")
public class TriggerEnt implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
    public int type;
    public int refId;
    public String triggerTime;
    public long startTime;
    public long endTime;
    public int status;
}
