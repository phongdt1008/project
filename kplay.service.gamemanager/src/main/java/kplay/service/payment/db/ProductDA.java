/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.payment.db;

import java.util.List;
import kplay.service.payment.ent.ProductEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author baotn
 */
@Repository
public interface ProductDA extends JpaRepository<ProductEnt, Integer> {

    @Query(value = "SELECT * FROM kplay.product where status = :status", nativeQuery = true)
    public List<ProductEnt> getCurrentListProduct(@Param(value = "status") int status);

    @Query(value = "SELECT * FROM kplay.product where type = :type and status = :status", nativeQuery = true)
    public List<ProductEnt> getCurrentListProduct(@Param(value = "type") int type, @Param(value = "status") int status);
}
