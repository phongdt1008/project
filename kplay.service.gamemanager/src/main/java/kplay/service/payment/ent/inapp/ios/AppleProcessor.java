/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.payment.ent.inapp.ios;

import kplay.service.payment.ent.StatusCode;
import kplay.service.payment.ent.VerifyResult;
import bliss.lib.framework.common.Config;
import bliss.lib.framework.common.LogUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import bliss.lib.framework.util.JSONUtil;
import bliss.lib.framework.util.NetworkUtils;
import ga.log4j.GA;
import kplay.service.gamemanager.config.ModuleConfig;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author baotn
 */
public class AppleProcessor {

    private static final String TAG = AppleProcessor.class.getName();

    private static String SANDBOX_URI = "https://sandbox.itunes.apple.com/verifyReceipt";
    private static String PRODUCTION_URI = "https://buy.itunes.apple.com/verifyReceipt";

    public static VerifyResult processPayment(String receipt, String appPackageId, long invoiceId) {
        GA.app.info("New Process");
        VerifyResult result = new VerifyResult();
        result.statusCode = StatusCode.unknown_error;
        String strResult = StringUtils.EMPTY;
        try {
            String pass = Config.getParam(ModuleConfig.IAP_CONFIG, "ios_client_secret");
            boolean sandbox = Config.getParam(ModuleConfig.IAP_CONFIG, "sandbox") == "1" ? true : false;

            String urlVerifyInapp = sandbox ? SANDBOX_URI : PRODUCTION_URI;

            ReceiptInfo receiptInfo = JSONUtil.DeSerialize(receipt.trim(), ReceiptInfo.class);

            String receiptData = String.format("{\"receipt-data\":\"%s\", \"password\":\"%s\"}", receiptInfo.Payload, pass);

            GA.app.info("New Process Url Verify Inapp: " + urlVerifyInapp);
            GA.app.info("Receipt Data: " + receiptData);

            strResult = NetworkUtils.post(urlVerifyInapp, receiptData);
            JsonObject obj = JSONUtil.DeSerialize(strResult, JsonObject.class);
            if (obj == null) {
                result.statusCode = StatusCode.unknown_error;
                return result;
            }

            GA.app.info("Result: \n" + obj.toString());
            int status = obj.get("status").getAsInt();
            if (status != 0) {
                if (status >= 21100 && status <= 21199) {
                    result.statusCode = StatusCode.status_code_21100_21199;
                } else {
                    result.statusCode = StatusCode.getStatusCode(status);
                }
                return result;
            }

            JsonArray arr = ((JsonObject) (obj.get("receipt"))).getAsJsonArray("in_app");

            InAppPurchaseResultInfo resultInfo = null;
            if (arr.size() > 0) {

                for (int i = 0; i < arr.size(); i++) {
                    JsonObject transaction_id_object = arr.get(i).getAsJsonObject();
                    String transaction_id = transaction_id_object.get("transaction_id").getAsString();

                    if (transaction_id.equalsIgnoreCase(receiptInfo.TransactionID)) {
                        resultInfo = JSONUtil.DeSerialize(arr.get(i).toString(), InAppPurchaseResultInfo.class);
                    }
                }
            }

            if (resultInfo == null) {
                result.statusCode = StatusCode.unknown_error;
                return result;
            }

            result.productKey = resultInfo.product_id;
            result.partnerId = resultInfo.transaction_id;
            result.statusCode = StatusCode.success;

        } catch (Exception ex) {
            result.statusCode = StatusCode.appstore_error;
            GA.app.error(TAG + LogUtil.stackTrace(ex));
            GA.app.error("Error with data receipt: " + receipt);
            GA.app.error("Error with data result : " + strResult);
            GA.app.error("Error with data invoiceId : " + invoiceId);
        }
        return result;
    }

    public static VerifyResult processPaymentSandbox(String receipt, String appPackageId, long invoiceId) {
        GA.app.info("New Process");
        VerifyResult result = new VerifyResult();
        result.statusCode = StatusCode.unknown_error;
        String strResult = StringUtils.EMPTY;
        try {
            String pass = Config.getParam(ModuleConfig.IAP_CONFIG, "ios_client_secret");

            String urlVerifyInapp = SANDBOX_URI;

            ReceiptInfo receiptInfo = JSONUtil.DeSerialize(receipt.trim(), ReceiptInfo.class);

            String receiptData = String.format("{\"receipt-data\":\"%s\", \"password\":\"%s\"}", receiptInfo.Payload, pass);

            GA.app.info("New Process Url Verify Inapp: " + urlVerifyInapp);
            GA.app.info("Receipt Data: " + receiptData);

            strResult = NetworkUtils.post(urlVerifyInapp, receiptData);
            JsonObject obj = JSONUtil.DeSerialize(strResult, JsonObject.class);
            if (obj == null) {
                result.statusCode = StatusCode.unknown_error;
                return result;
            }

            GA.app.info("Result: \n" + obj.toString());
            int status = obj.get("status").getAsInt();
            if (status != 0) {
                if (status >= 21100 && status <= 21199) {
                    result.statusCode = StatusCode.status_code_21100_21199;
                } else {
                    result.statusCode = StatusCode.getStatusCode(status);
                }
                return result;
            }

            JsonArray arr = ((JsonObject) (obj.get("receipt"))).getAsJsonArray("in_app");

            InAppPurchaseResultInfo resultInfo = null;
            if (arr.size() > 0) {

                for (int i = 0; i < arr.size(); i++) {
                    JsonObject transaction_id_object = arr.get(i).getAsJsonObject();
                    String transaction_id = transaction_id_object.get("transaction_id").getAsString();

                    if (transaction_id.equalsIgnoreCase(receiptInfo.TransactionID)) {
                        resultInfo = JSONUtil.DeSerialize(arr.get(i).toString(), InAppPurchaseResultInfo.class);
                    }
                }
            }

            if (resultInfo == null) {
                result.statusCode = StatusCode.unknown_error;
                return result;
            }

            result.productKey = resultInfo.product_id;
            result.partnerId = resultInfo.transaction_id;
            result.statusCode = StatusCode.success;

        } catch (Exception ex) {
            result.statusCode = StatusCode.appstore_error;
            GA.app.error(TAG + LogUtil.stackTrace(ex));
            GA.app.error("Error with data receipt: " + receipt);
            GA.app.error("Error with data result : " + strResult);
            GA.app.error("Error with data invoiceId : " + invoiceId);
        }
        return result;
    }
}
