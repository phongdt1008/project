/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.payment.ent.inapp.ios;

/**
 *
 * @author baotn
 */
public class InAppPurchaseResultInfo
{
    public int quantity;
    public String product_id;
    public String transaction_id;
    public String original_transaction_id;
    public String purchase_date;
    public String purchase_date_ms;
    public String purchase_date_pst;
    public String original_purchase_date;
    public String original_purchase_date_ms;
    public String original_purchase_date_pst;
    public boolean is_trial_period;
}
