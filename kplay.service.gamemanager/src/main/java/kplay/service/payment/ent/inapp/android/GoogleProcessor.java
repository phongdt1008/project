/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.payment.ent.inapp.android;

import kplay.service.payment.ent.StatusCode;
import kplay.service.payment.ent.VerifyResult;
import bliss.lib.framework.common.Config;
import bliss.lib.framework.common.LogUtil;
import bliss.lib.framework.util.JSONUtil;
import bliss.lib.framework.util.NetworkUtils;
import ga.log4j.GA;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import kplay.service.gamemanager.config.ModuleConfig;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author baotn
 */
public class GoogleProcessor {

    private static final String TAG = GoogleProcessor.class.getName();

    private final static String URL_CHECK_PAYMENT_GOOGLE = "https://www.googleapis.com/androidpublisher/v3/applications";
    private final static String GET_TOKEN_GOOGLE = "https://accounts.google.com/o/oauth2/token";
    private final static String CARDPAY_QUERY_TEMPLATE = "/%s/purchases/products/%s/tokens/%s?access_token=%s";

    public static VerifyResult processPayment(String receipt, String appPackageId, long invoiceId) {
        VerifyResult result = new VerifyResult();
        result.statusCode = StatusCode.unknown_error;
        String strResultPayment = StringUtils.EMPTY;
        try {
            ReceiptInfo purchaseInfo = JSONUtil.DeSerialize(receipt, ReceiptInfo.class);
            result.productKey = purchaseInfo.productId;

            GA.app.info("Receipt Data: " + receipt);

            String accessToken = null;
            if (accessToken == null || accessToken.isEmpty()) {
                String strResult = getAccessToken(purchaseInfo.packageName);
                AccessToken access_token = JSONUtil.DeSerialize(strResult, AccessToken.class);
                if (access_token != null) {
                    accessToken = access_token.access_token;
                }
            }

            GA.app.info("AccessToken: " + accessToken);

            if (accessToken != null) {
                String url = URL_CHECK_PAYMENT_GOOGLE + String.format(CARDPAY_QUERY_TEMPLATE, purchaseInfo.packageName, purchaseInfo.productId, purchaseInfo.purchaseToken, accessToken);
                url = url.replaceAll(" ", "%20");
                strResultPayment = NetworkUtils.getResponse(url);
                if (strResultPayment != null) {
                    GA.app.info("Result: \n" + strResultPayment);
                    InAppPurchaseResultInfo resultInfo = JSONUtil.DeSerialize(strResultPayment, InAppPurchaseResultInfo.class);
                    if (resultInfo != null) {
                        result.partnerId = resultInfo.orderId;
                        result.statusCode = StatusCode.success;
                    }
                }
            } else {
                result.statusCode = StatusCode.unknown_error;
            }
        } catch (Exception ex) {
            result.statusCode = StatusCode.appstore_error;
            GA.app.error(TAG + LogUtil.stackTrace(ex));
            GA.app.error("Error with data receipt: " + receipt);
            GA.app.error("Error with data result : " + strResultPayment);
            GA.app.error("Error with data invoiceId : " + invoiceId);
        }
        return result;
    }

    public static String getAccessToken(String packageName) {
        String access_token = StringUtils.EMPTY;

        String refresh_token = Config.getParam(ModuleConfig.IAP_CONFIG, "refresh_token");
        String client_id = Config.getParam(ModuleConfig.IAP_CONFIG, "client_id");
        String client_secret = Config.getParam(ModuleConfig.IAP_CONFIG, "client_secret");

        if (StringUtils.isBlank(refresh_token) || StringUtils.isBlank(client_id) || StringUtils.isBlank(client_secret)) {
            return StringUtils.EMPTY;
        }

        String url = GET_TOKEN_GOOGLE;

        try {
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String urlParameters = "grant_type=refresh_token" + "&"
                    + "refresh_token=" + refresh_token + "&"
                    + "client_id=" + client_id + "&"
                    + "client_secret=" + client_secret;

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = rd.readLine()) != null) {
                response.append(inputLine);
            }
            rd.close();

            access_token = response.toString();
        } catch (Exception ex) {
            access_token = StringUtils.EMPTY;
            GA.app.error(GoogleProcessor.class.getName(), ex);
        }
        return access_token;
    }
}
