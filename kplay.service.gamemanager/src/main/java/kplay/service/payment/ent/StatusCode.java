/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.payment.ent;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author baotn
 */
public enum StatusCode {
    success(0, "Success."), // Android success | Apple success | Card success
    unknown_error(-1, "Unknown error, try again."),
    incorrect_product_id(-2, "Incorrect product id."),
    partner_id_exist(-3, "Partner id exist."),
    appstore_error(-4, "Appstore error."),
    // Apple status code
    status_code_21000(21000, "The App Store could not read the JSON object you provided."),
    status_code_21002(21002, "The data in the receipt-data property was malformed or missing."),
    status_code_21003(21003, "The receipt could not be authenticated."),
    status_code_21004(21004, "The shared secret you provided does not match the shared secret on file for your account."),
    status_code_21005(21005, "The receipt server is not currently available."),
    status_code_21006(21006, "This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response.\n"
            + "Only returned for iOS 6 style transaction receipts for auto-renewable subscriptions."),
    status_code_21007(21007, "This receipt is from the test environment, but it was sent to the production environment for verification. Send it to the test environment instead."),
    status_code_21008(21008, "This receipt is from the production environment, but it was sent to the test environment for verification. Send it to the production environment instead."),
    status_code_21010(21010, "This receipt could not be authorized. Treat this the same as if a purchase was never made."),
    status_code_21100_21199(2110021199, "Internal data access error.");

    public static String translateStatusCode(String error) {
        StatusCode[] codes = StatusCode.values();
        for (int i = 0; i < codes.length; i++) {
            if (codes[i].getStringValue().equalsIgnoreCase(error)) {
                return codes[i].toString();
            }
        }
        return StringUtils.EMPTY;
    }

    public static StatusCode getStatusCode(int intValue) {
        StatusCode[] codes = StatusCode.values();
        for (int i = 0; i < codes.length; i++) {
            if (codes[i].getIntValue() == intValue) {
                return codes[i];
            }
        }
        return StatusCode.unknown_error;
    }

    public static StatusCode getStatusCode(String strValue) {
        StatusCode[] codes = StatusCode.values();
        for (int i = 0; i < codes.length; i++) {
            if (codes[i].getStringValue().equalsIgnoreCase(strValue)) {
                return codes[i];
            }
        }
        return StatusCode.unknown_error;
    }

    private int intValue;
    private String description;

    private StatusCode(int intValue, String description) {
        this.intValue = intValue;
        this.description = description;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public String getStringValue() {
        return this.intValue + StringUtils.EMPTY;
    }

    public String getDescription() {
        return this.description;
    }
}
