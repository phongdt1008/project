/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.payment.ent.inapp.android;

/**
 *
 * @author baotn
 */
public class InAppPurchaseResultInfo
{
    public String kind;
    public long purchaseTimeMillis;
    public int purchaseState;
    public int consumptionState;
    public String developerPayload;
    public String orderId;
    public int purchaseType;  
}
