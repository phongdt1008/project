/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.payment.bc;

import bliss.lib.framework.common.LogUtil;
import ga.log4j.GA;
import java.util.List;
import kplay.service.payment.db.ProductDA;
import kplay.service.payment.ent.ProductEnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author baotn
 */
@Service
public class ProductBC {

    @Autowired
    public ProductDA productDA;

    public ProductEnt addProduct(ProductEnt product) {
        productDA.save(product);
        return product;
    }

    public ProductEnt getProduct(int id) {
        return productDA.getOne(id);
    }

    public List<ProductEnt> getCurrentListProduct(int status) {
        return productDA.getCurrentListProduct(status);
    }

    public List<ProductEnt> getCurrentListProduct(int type, int status) {
        return productDA.getCurrentListProduct(type, status);
    }

    public boolean updateProduct(ProductEnt product) {
        try {
            ProductEnt dbEnt = productDA.getOne(product.getId());
            dbEnt.setKeyId(product.getKeyId());
            dbEnt.setMoney(product.getMoney());
            dbEnt.setRealMoney(product.getRealMoney());
            dbEnt.setStatus(product.getStatus());
            dbEnt.setType(product.getType());

            productDA.save(dbEnt);
            return true;
        } catch (Exception e) {
            GA.app.error(LogUtil.stackTrace(e));
        }
        return true;
    }
}
