/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.schedule;

import bliss.lib.framework.common.Config;
import java.util.Arrays;
import java.util.List;
import kplay.service.leaderboard.bc.LeaderBoardBC;
import kplay.service.leaderboard.bc.RewardBC;
import kplay.service.leaderboard.ent.LeaderBoardEnt;
import kplay.service.leaderboard.ent.LeaderBoardType;
import kplay.service.leaderboard.redis.LeaderBoardCA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author phong
 */
@Component
public class ScheduledTasks {

    @Autowired
    private LeaderBoardBC leaderBoardBC;

    private final String listGameString = Config.getParam("leader_board_config", "list_game_id");
    private final String listBoardString = Config.getParam("leader_board_config", "list_board_type");

    private final int[] listGameId = Arrays.stream(listGameString.split(",")).mapToInt(Integer::parseInt).toArray();
    private final int[] listBoard = Arrays.stream(listBoardString.split(",")).mapToInt(Integer::parseInt).toArray();

    /**
     * Move all current leader board to db, reset leader board
     */
    @Scheduled(zone = "Asia/Ho_Chi_Minh", cron = "0 59 9 ? * MON *")
    public void moveRedisToDB() {
        for (int gameId : listGameId) {
            for (int board : listBoard) {
                List<LeaderBoardEnt> allLeader = LeaderBoardCA.getAllLeader(gameId, board);
                RewardBC.reward(allLeader, board, gameId);
                leaderBoardBC.saveAllEnt(allLeader);

                if (board == LeaderBoardType.TOP_PAYMENT.getValue()) {
                    LeaderBoardCA.resetBoard(gameId, board);
                }
            }
        }
    }
}
