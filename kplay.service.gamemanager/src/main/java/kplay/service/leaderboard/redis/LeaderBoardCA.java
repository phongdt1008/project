/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.redis;

import bliss.lib.framework.common.Config;
import bliss.lib.framework.common.LogUtil;
import bliss.lib.framework.redis.RedisFactory;
import ga.log4j.GA;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kplay.service.gamemanager.config.ModuleConfig;
import kplay.service.leaderboard.ent.LeaderBoardEnt;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

/**
 *
 * @author phong
 */
public class LeaderBoardCA {

    public static final String LEADERBOARD_NAME = "leaderboard_%s_%s";
    public static final String TAX_NAME = "tax_game_%s";
    public static final int RETURN_COUNT = Integer.parseInt(Config.getParam("leader_board_config", "return_count"));

    Jedis jedis = null;
    RedisFactory client = null;

    public static void init() {

    }

    public static void increaseRecord(int userId, int gameId, double value, int leaderBoardType) {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();
            String key = String.format(LEADERBOARD_NAME, gameId, leaderBoardType);
            String memeber = String.valueOf(userId);

            Double oldScore = jedis.zscore(key, memeber);

            if (null != oldScore) {
                jedis.zincrby(key, value, memeber);
            } else {
                Map<String, Double> scoreMembers = new HashMap<String, Double>();
                scoreMembers.put(memeber, value);
                jedis.zadd(key, scoreMembers);
            }

        } catch (Exception ex) {
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
    }

    public static List<LeaderBoardEnt> getTopLeader(int gameId, int leaderBoardType) {
        Jedis jedis = null;
        RedisFactory client = null;
        Set<Tuple> userWithScore;
        List<LeaderBoardEnt> leaderBoard;
        String key = String.format(LEADERBOARD_NAME, gameId, leaderBoardType);
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();
            userWithScore = jedis.zrangeWithScores(key, 0, RETURN_COUNT);
            leaderBoard = new ArrayList<>();

            for (Tuple tuple : userWithScore) {
                LeaderBoardEnt ent = new LeaderBoardEnt();
                ent.setGameId(gameId);
                ent.setType(leaderBoardType);
                ent.setScore(tuple.getScore());
                ent.setUserId(Integer.parseInt(tuple.getElement()));

                leaderBoard.add(ent);
            }
            return leaderBoard;
        } catch (Exception e) {
            GA.app.error(LogUtil.stackTrace(e));
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
        return Collections.emptyList();
    }

    public static List<LeaderBoardEnt> getAllLeader(int gameId, int leaderBoardType) {
        Jedis jedis = null;
        RedisFactory client = null;
        Set<Tuple> userWithScore;
        List<LeaderBoardEnt> leaderBoard;
        String key = String.format(LEADERBOARD_NAME, gameId, leaderBoardType);
        long time = new Date().getTime();
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();
            userWithScore = jedis.zrangeWithScores(key, 0, -1);
            leaderBoard = new ArrayList<>();

            for (Tuple tuple : userWithScore) {
                LeaderBoardEnt ent = new LeaderBoardEnt();
                ent.setGameId(gameId);
                ent.setType(leaderBoardType);
                ent.setScore(tuple.getScore());
                ent.setUserId(Integer.parseInt(tuple.getElement()));
                ent.setTime(time);

                leaderBoard.add(ent);
            }
            return leaderBoard;
        } catch (Exception e) {
            GA.app.error(LogUtil.stackTrace(e));
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
        return Collections.emptyList();
    }

    public static void resetBoard(int gameId, int leaderBoardType) {
        Jedis jedis = null;
        RedisFactory client = null;
        String key = String.format(LEADERBOARD_NAME, gameId, leaderBoardType);
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();

            jedis.del(key);
        } catch (Exception e) {
            GA.app.error(LogUtil.stackTrace(e));
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
    }

    public static void increaseTax(int gameId, long value) {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();
            String key = String.format(TAX_NAME, gameId);

            String oldScore = jedis.get(key);

            if (null != oldScore) {
                jedis.incrBy(key, value);
            } else {
                jedis.set(key, String.valueOf(value));
            }

        } catch (Exception ex) {
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
    }

    public static long getTax(int gameId) {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();
            String key = String.format(TAX_NAME, gameId);

            String oldScore = jedis.get(key);

            if (null != oldScore) {
                return Long.parseLong(oldScore);
            }
        } catch (Exception ex) {
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
        return 0;
    }

    public static long resetTax(int gameId) {
        Jedis jedis = null;
        RedisFactory client = null;
        try {
            client = RedisFactory.getInstance(ModuleConfig.REDIS_CONFIG);
            jedis = client.getClient();
            String key = String.format(TAX_NAME, gameId);
            jedis.del(key);
        } catch (Exception ex) {
        } finally {
            if (jedis != null && client != null) {
                client.returnClient(jedis);
            }
        }
        return 0;
    }
}
