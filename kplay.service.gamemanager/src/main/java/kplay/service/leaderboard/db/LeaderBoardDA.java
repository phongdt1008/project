/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.db;

import java.sql.Timestamp;
import java.util.List;
import kplay.service.leaderboard.ent.LeaderBoardEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author phong
 */
@Repository
public interface LeaderBoardDA extends JpaRepository<LeaderBoardEnt, Integer> {

    @Query(value = "select * from kplay.leaderboard "
            + "where time >= :from and time <= :to and gameId = :gameId and type = :leaderBoardType order by score desc", nativeQuery = true)
    List<LeaderBoardEnt> findListByTime(@Param(value = "from") Timestamp from,
            @Param(value = "to") Timestamp to,
            @Param(value = "gameId") int gameId,
            @Param(value = "leaderBoardType") int leaderBoardType);

}
