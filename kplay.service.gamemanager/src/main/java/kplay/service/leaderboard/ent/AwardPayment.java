/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.ent;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author phong
 */
public enum AwardPayment {
    NO_AWARD(0),
    TOP_1_AVATAR(1),
    TOP_1_BADGE(2),
    TOP_2_AVATAR(3),
    TOP_2_BADGE(4),
    TOP_3_AVATAR(5),
    TOP_3_BADGE(6),
    OTHER_AVATAR(7),
    OTHER_BADGE(8);

    private final int value;

    private AwardPayment(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static AwardPayment findByValue(int value) {
        switch (value) {
            case 0:
                return NO_AWARD;
            case 1:
                return TOP_1_AVATAR;
            case 2:
                return TOP_1_BADGE;
            case 3:
                return TOP_2_AVATAR;
            case 4:
                return TOP_2_BADGE;
            case 5:
                return TOP_3_AVATAR;
            case 6:
                return TOP_3_BADGE;
            case 7:
                return OTHER_AVATAR;
            case 8:
                return OTHER_BADGE;
            default:
                return null;
        }
    }
}
