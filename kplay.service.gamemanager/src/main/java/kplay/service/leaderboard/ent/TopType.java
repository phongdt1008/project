/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.ent;

/**
 *
 * @author phong
 */
public enum TopType {
    TOP_1(0),
    TOP_2(1),
    TOP_3(2),
    OTHER(3);

    private final int value;

    private TopType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static TopType findByValue(int value) {
        switch (value) {
            case 0:
                return TOP_1;
            case 1:
                return TOP_2;
            case 2:
                return TOP_3;
            case 3:
                return OTHER;
            default:
                return null;
        }
    }
}
