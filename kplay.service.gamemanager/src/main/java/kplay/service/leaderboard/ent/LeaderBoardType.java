/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.ent;

/**
 *
 * @author phong
 */
public enum LeaderBoardType {
    TOP_WIN(0),
    TOP_PAYMENT(1);

    private final int value;

    private LeaderBoardType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static LeaderBoardType findByValue(int value) {
        switch (value) {
            case 0:
                return TOP_WIN;
            case 1:
                return TOP_PAYMENT;
            default:
                return null;
        }
    }
}
