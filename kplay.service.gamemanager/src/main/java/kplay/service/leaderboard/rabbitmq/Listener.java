/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.rabbitmq;

import bliss.lib.framework.common.Config;
import kplay.service.leaderboard.redis.LeaderBoardCA;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 *
 * @author phong
 */
@Component
public class Listener {

    private static final String increaseScoreQueue = Config.getParam("leader_board_config", "increase_score_queue");

    @Bean
    public Queue myQueue() {
        return new Queue(increaseScoreQueue, false);
    }

    @RabbitListener(queues = "${increase_score_queue}")
    public void listen(String in) {
        LeaderBoardCA.increaseRecord(0, 0, 0, 0);
    }
}
