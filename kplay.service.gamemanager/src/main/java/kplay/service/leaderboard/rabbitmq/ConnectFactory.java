/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.rabbitmq;

import bliss.lib.framework.common.Config;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author phong
 */
public class ConnectFactory {

    private static Connection instance;

    private static final String HOST = Config.getParam("rabbitmq_config", "host");
    private static final int PORT = Integer.parseInt(Config.getParam("rabbitmq_config", "host"));
    private static final String USERNAME = Config.getParam("rabbitmq_config", "host");
    private static final String PASSWORD = Config.getParam("rabbitmq_config", "host");

    public static Connection getInstance() {
        if (instance == null) {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(HOST);
            factory.setPort(PORT);
            factory.setUsername(USERNAME);
            factory.setPassword(PASSWORD);
            try {
                instance = factory.newConnection();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }
}
