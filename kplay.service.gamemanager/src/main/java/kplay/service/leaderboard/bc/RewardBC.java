/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.bc;

import bliss.lib.framework.common.Config;
import java.util.Arrays;
import java.util.List;
import kplay.service.leaderboard.ent.LeaderBoardEnt;
import kplay.service.leaderboard.ent.LeaderBoardType;
import kplay.service.leaderboard.redis.LeaderBoardCA;

/**
 *
 * @author phong
 */
public class RewardBC {

    private static final String TOP1_PAYMENT_AWARD_STRING = Config.getParam("leader_board_config", "top_1_payment");
    private static final String TOP2_PAYMENT_AWARD_STRING = Config.getParam("leader_board_config", "top_2_payment");
    private static final String TOP3_PAYMENT_AWARD_STRING = Config.getParam("leader_board_config", "top_3_payment");
    private static final String TOP4_PAYMENT_AWARD_STRING = Config.getParam("leader_board_config", "top_4_payment");

    private static final int[] TOP1_PAYMENT_AWARD = Arrays.stream(TOP1_PAYMENT_AWARD_STRING.split(",")).mapToInt(Integer::parseInt).toArray();
    private static final int[] TOP2_PAYMENT_AWARD = Arrays.stream(TOP2_PAYMENT_AWARD_STRING.split(",")).mapToInt(Integer::parseInt).toArray();
    private static final int[] TOP3_PAYMENT_AWARD = Arrays.stream(TOP3_PAYMENT_AWARD_STRING.split(",")).mapToInt(Integer::parseInt).toArray();
    private static final int[] TOP4_PAYMENT_AWARD = Arrays.stream(TOP4_PAYMENT_AWARD_STRING.split(",")).mapToInt(Integer::parseInt).toArray();

    private static final boolean IS_USE_TAX = Boolean.parseBoolean(Config.getParam("leader_board_config", "use_tax"));
    private static final int PERCENT_TAX = Integer.parseInt(Config.getParam("leader_board_config", "percent_tax"));
    private static final long STATIC_NCOIN_AWARD = Long.parseLong(Config.getParam("leader_board_config", "ncoin_award"));

    private static final int TOP1_AWARD_PERCENT = Integer.parseInt(Config.getParam("leader_board_config", "top_1_percent"));
    private static final int TOP2_AWARD_PERCENT = Integer.parseInt(Config.getParam("leader_board_config", "top_2_percent"));
    private static final int TOP3_AWARD_PERCENT = Integer.parseInt(Config.getParam("leader_board_config", "top_3_percent"));
    private static final int TOP4_AWARD_PERCENT = Integer.parseInt(Config.getParam("leader_board_config", "top_4_percent"));

    /**
     * Add award to top 20 user
     *
     * @param topLeader
     * @param board
     * @param gameId
     * @return
     */
    public static List<LeaderBoardEnt> reward(List<LeaderBoardEnt> topLeader, int board, int gameId) {

        if (board == LeaderBoardType.TOP_PAYMENT.getValue()) {
            topLeader.get(0).setAvatar(TOP1_PAYMENT_AWARD[0]);
            topLeader.get(0).setBadge(TOP1_PAYMENT_AWARD[1]);

            topLeader.get(1).setAvatar(TOP2_PAYMENT_AWARD[0]);
            topLeader.get(1).setBadge(TOP2_PAYMENT_AWARD[1]);

            topLeader.get(2).setAvatar(TOP3_PAYMENT_AWARD[0]);
            topLeader.get(2).setBadge(TOP3_PAYMENT_AWARD[1]);

            for (int i = 3; i < topLeader.size(); i++) {
                topLeader.get(i).setAvatar(TOP4_PAYMENT_AWARD[0]);
            }
        } else if (board == LeaderBoardType.TOP_WIN.getValue()) {
            long totalAward = 0;

            if (IS_USE_TAX) {
                long tax = LeaderBoardCA.getTax(gameId);
                totalAward = tax / 100 * PERCENT_TAX;

            } else {
                totalAward = STATIC_NCOIN_AWARD;
            }

            long top1_award = totalAward / 100 * TOP1_AWARD_PERCENT;
            long top2_award = totalAward / 100 * TOP2_AWARD_PERCENT;
            long top3_award = totalAward / 100 * TOP3_AWARD_PERCENT;
            long top4_award = totalAward / 100 * TOP4_AWARD_PERCENT;

            topLeader.get(0).setNcoin_award(top1_award);
            topLeader.get(1).setNcoin_award(top2_award);
            topLeader.get(2).setNcoin_award(top3_award);
            for (int i = 3; i < topLeader.size(); i++) {
                topLeader.get(i).setNcoin_award(top4_award);
            }
        }
        return topLeader;
    }

}
