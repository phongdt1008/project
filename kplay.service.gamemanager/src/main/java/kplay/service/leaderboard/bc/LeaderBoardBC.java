/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kplay.service.leaderboard.bc;

import bliss.lib.framework.common.LogUtil;
import bliss.lib.framework.util.DateTimeUtils;
import ga.log4j.GA;
import java.util.Collections;
import java.util.List;
import kplay.service.leaderboard.db.LeaderBoardDA;
import kplay.service.leaderboard.ent.LeaderBoardEnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author phong
 */
@Service
public class LeaderBoardBC {

    @Autowired
    private LeaderBoardDA leaderBoardDA;

    public void saveAllEnt(List<LeaderBoardEnt> leaderBoardEnts) {
        try {
            leaderBoardDA.saveAll(leaderBoardEnts);
        } catch (Exception e) {
            GA.app.error(LogUtil.stackTrace(e));
        }
    }

    public List<LeaderBoardEnt> findLeaderBoard(long from, long to, int gameId, int leaderBoardType) {
        try {
            return leaderBoardDA.findListByTime(DateTimeUtils.getTimestampSql(from), DateTimeUtils.getTimestampSql(to), gameId, leaderBoardType);
        } catch (Exception e) {
            GA.app.error(LogUtil.stackTrace(e));
        }
        return Collections.emptyList();
    }
}
